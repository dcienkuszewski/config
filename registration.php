<?php

use Gnom\Application\Component\ComponentRegistrar;

ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Gnom_Config', __DIR__);