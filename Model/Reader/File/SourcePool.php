<?php

declare(strict_types=1);

namespace Gnom\Config\Model\Reader\File;

use Gnom\Application\ObjectManagerInterface;
use Gnom\Config\Api\SourceInterface;
use Gnom\Config\Api\SourceProviderInterface;

class SourcePool
{
    /**
     * @var string[]
     */
    private array $providers;

    /**
     * @var SourceInterface[]
     */
    private ?array $sources = null;

    private ObjectManagerInterface $objectManager;

    public function __construct(ObjectManagerInterface $objectManager, array $providers = [])
    {
        $this->objectManager = $objectManager;
        $this->providers = $providers;
    }

    /**
     * @return SourceInterface[]
     */
    public function getSources(): array
    {
        if ($this->sources === null) {
            $this->sources = [];
            foreach ($this->providers as $className) {
                $object = $this->objectManager->get($className);
                if ($object instanceof SourceProviderInterface) {
                    $this->sources = array_merge($this->sources, $object->provide());
                }
            }
        }

        return $this->sources;
    }
}