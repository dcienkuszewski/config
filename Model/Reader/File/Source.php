<?php

declare(strict_types=1);

namespace Gnom\Config\Model\Reader\File;

use Gnom\Config\Api\SourceInterface;

class Source implements SourceInterface
{
    private string $filename;

    public function __construct(string $filename)
    {
        $this->filename = $filename;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }
}