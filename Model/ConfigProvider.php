<?php

declare(strict_types=1);

namespace Gnom\Config\Model;

use Gnom\Config\Api\ConfigProviderInterface;
use Gnom\Application\ArrayManager;
use Gnom\Config\Api\ReaderInterface;

class ConfigProvider implements ConfigProviderInterface
{
    private ArrayManager $arrayManager;

    private ReaderInterface$configReader;

    private ?array $data = null;

    public function __construct(ArrayManager $arrayManager, ReaderInterface $configReader)
    {
        $this->arrayManager = $arrayManager;
        $this->configReader = $configReader;
    }

    /**
     * {@inheritDoc}
     * @noinspection PhpMissingReturnTypeInspection
     */
    public function getValue(string $path)
    {
        return $this->arrayManager->get($path, $this->getData());
    }

    private function getData(): array {
        if ($this->data === null){
            $this->data = $this->configReader->read();
        }

        return $this->data;
    }
}