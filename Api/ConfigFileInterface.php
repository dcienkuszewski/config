<?php

declare(strict_types=1);

namespace Gnom\Config\Api;

interface ConfigFileInterface
{
    public const FILE_PATH_PATTERN = 'etc/config.yaml';
}