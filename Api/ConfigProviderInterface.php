<?php

declare(strict_types=1);

namespace Gnom\Config\Api;

interface ConfigProviderInterface
{
    /**
     * @param string $path
     *
     * @return mixed
     */
    public function getValue(string $path);
}